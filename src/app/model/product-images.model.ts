export class ProductImage{
    constructor(
        public id:string,
        public image:string,
        public name:string
    ){}
}