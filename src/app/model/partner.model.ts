import { StringMap } from '@angular/compiler/src/compiler_facade_interface';

export class Partner{
    constructor(
        public id:string,
        public name:String,
        public address:string,
        public phone:string,
        public logo:string,
        public active:boolean
    ){}
}