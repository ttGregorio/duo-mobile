import { ProductType } from './product-type.model';

export class UserPreferenceProductType{
    constructor(
        public username:string,
        public productType:ProductType
    ){}
}