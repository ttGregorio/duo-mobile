export class ProductSearchDTO{
    constructor(
        public productName:string,
        public partnerName:string,
        public productType:string
    ){}
}