import { ProductType } from './product-type.model';
import { Partner } from './partner.model';
import { ProductSubType } from './product-sub-type.model';
import { ProductImage } from './product-images.model';

export class 
Product{
    constructor(
        public id:string,
        public name:string,
        public description:string,
        public value:number,
        public partner:Partner,
        public productType:ProductType,
        public productSubType:ProductSubType,
        public productImages:ProductImage[],
        public active:boolean
    ){}
}