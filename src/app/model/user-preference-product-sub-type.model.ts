import { ProductSubType } from './product-sub-type.model';

export class UserPreferenceProductSubType{
    constructor(
        public username:string,
        public productSubType:ProductSubType
    ){}
}