import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ENDPOINT_API } from './nex2you.api';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {

  constructor(private http:HttpClient) { }

  findAllList(){
    return this.http.get(`${ENDPOINT_API}/partners`);
  }
}
