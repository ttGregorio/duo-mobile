import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserPreferenceProductSubType } from '../model/user-preference-product-sub-type.model';
import { ENDPOINT_API } from './nex2you.api';

@Injectable({
  providedIn: 'root'
})
export class UserPreferenceProductSubTypeService {

  constructor(private http:HttpClient) { }

  createOrUpdate(userPreferences:UserPreferenceProductSubType){
    return this.http.post(`${ENDPOINT_API}/user-product-sub-types-preferences`, userPreferences);
  }

  findAllList(){
    return this.http.get(`${ENDPOINT_API}/user-product-sub-types-preferences`);
  }

  findByUsername(username:string){
    return this.http.get(`${ENDPOINT_API}/user-product-sub-types-preferences/${username}/username`);
  }

  delete(productTypeId:string,username:string){
    return this.http.delete(`${ENDPOINT_API}/user-product-sub-types-preferences/${productTypeId}/${username}`);
  }
}
