import { TestBed } from '@angular/core/testing';

import { UserPreferenceProductTypeService } from './user-preference-product-type.service';

describe('UserPreferenceProductTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserPreferenceProductTypeService = TestBed.get(UserPreferenceProductTypeService);
    expect(service).toBeTruthy();
  });
});
