import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductSubType } from '../model/product-sub-type.model';
import { ENDPOINT_API } from './nex2you.api';

@Injectable({
  providedIn: 'root'
})
export class ProductSubTypeService {

  constructor(private http:HttpClient) { }

  createOrUpdate(productType:ProductSubType){
    if(productType.id != null && productType.id != ""){
      return this.http.put(`${ENDPOINT_API}/product-sub-types`, productType);
    }else{
      productType.id = null;
      return this.http.post(`${ENDPOINT_API}/product-sub-types`, productType);
    }
  }

  findAllList(){
    return this.http.get(`${ENDPOINT_API}/product-sub-types`);
  }

  findByUsername(username:string,productTypeId:string){
    return this.http.get(`${ENDPOINT_API}/product-sub-types/${productTypeId}/${username}/username`);
  }

  findById(id:string){
    return this.http.get(`${ENDPOINT_API}/product-sub-types/${id}`);
  }
}
