import { Injectable } from '@angular/core';
import { ENDPOINT_API } from './nex2you.api';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductImageService {


  constructor(private http:HttpClient) { }


  findAllList(productId:string){
    return this.http.get(`${ENDPOINT_API}/product-images/${productId}`);
  }

  findByName(productId:string,name:string){
    return this.http.get(`${ENDPOINT_API}/product-images/${productId}/${name}/image`);
  }

  findFirstImage(productId:string){
    return this.http.get(`${ENDPOINT_API}/product-images/${productId}/first-image`);
  }

}
