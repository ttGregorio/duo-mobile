import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Order } from '../model/order.model';
import { ENDPOINT_API } from './nex2you.api';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  
  constructor(private http:HttpClient) { }

  createOrUpdate(order:Order){
    order.id = null;
    return this.http.post(`${ENDPOINT_API}/orders`, order);
  }

  findById(id:string){
    return this.http.get(`${ENDPOINT_API}/orders/${id}/id`);
  }

  findByUsername(page:number, count:number,username:string){
    return this.http.get(`${ENDPOINT_API}/orders/${page}/${count}/${username}/user`);
  }
}
