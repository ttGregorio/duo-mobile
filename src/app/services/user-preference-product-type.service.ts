import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserPreferenceProductType } from '../model/user-preference-product-type.model';
import { ENDPOINT_API } from './nex2you.api';

@Injectable({
  providedIn: 'root'
})
export class UserPreferenceProductTypeService {

  constructor(private http:HttpClient) { }

  createOrUpdate(userPreferences:UserPreferenceProductType){
    return this.http.post(`${ENDPOINT_API}/user-product-types-preferences`, userPreferences);
  }

  findAllList(){
    return this.http.get(`${ENDPOINT_API}/user-product-types-preferences`);
  }

  findByUsername(username:string){
    return this.http.get(`${ENDPOINT_API}/user-product-types-preferences/${username}/username`);
  }

  delete(productTypeId:string,username:string){
    return this.http.delete(`${ENDPOINT_API}/user-product-types-preferences/${productTypeId}/${username}`);
  }
}
