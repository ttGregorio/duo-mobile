import { TestBed } from '@angular/core/testing';

import { UserPreferenceProductSubTypeService } from './user-preference-product-sub-type.service';

describe('UserPreferenceProductSubTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserPreferenceProductSubTypeService = TestBed.get(UserPreferenceProductSubTypeService);
    expect(service).toBeTruthy();
  });
});
