import { TestBed } from '@angular/core/testing';

import { ProductSubTypeService } from './product-sub-type.service';

describe('ProductSubTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductSubTypeService = TestBed.get(ProductSubTypeService);
    expect(service).toBeTruthy();
  });
});
