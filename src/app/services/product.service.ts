import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ENDPOINT_API } from './nex2you.api';
import { ProductSearchDTO } from '../model/product-search-dto.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http:HttpClient) { }

  findAllList(){
    return this.http.get(`${ENDPOINT_API}/products`);
  }

  findByUsername(page:number, count:number, productTypeId:string, username:string, productSearchDTO:ProductSearchDTO){
    return this.http.post(`${ENDPOINT_API}/products/${page}/${count}/${username}/${productTypeId}/search`, productSearchDTO);
  }

  findById(id:string){
    return this.http.get(`${ENDPOINT_API}/products/${id}/id`);
  }
}