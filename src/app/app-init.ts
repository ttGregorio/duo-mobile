import { KeycloakService } from 'keycloak-angular';

export function initializer(keycloak: KeycloakService): () => Promise<any> {
  return (): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      try {
        await keycloak.init({
          config: {
            url: 'http://localhost:8080/auth',
            realm: 'weareone',
            clientId: 'login-app',
            credentials: {
              "secret": "6ee00361-45ea-4be9-b3fe-b06788ed9a2f"
            }
          },
          initOptions: {
            onLoad: 'login-required',
            checkLoginIframe: false
          },
          enableBearerInterceptor: true,
          bearerExcludedUrls: ['/assets']
        });
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  };
}
