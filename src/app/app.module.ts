import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { KeycloakService, KeycloakAngularModule } from 'keycloak-angular';
import { initializer } from './app-init';
import { HttpClient } from '@angular/common/http';
import { registerLocaleData, CommonModule } from '@angular/common'; 
import localePt from '@angular/common/locales/pt-PT';  
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { UserInfoComponent } from './components/user/user-info/user-info.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { UserPreferencesListComponent } from './components/user/user-preferences/user-preferences-list/user-preferences-list.component';
import { UserPreferencesSubListComponent } from './components/user/user-preferences/user-preferences-sub-list/user-preferences-sub-list.component';
import { ProductsListComponent } from './components/products/products-list/products-list.component';
import { ProductListItemComponent } from './components/products/products-list/product-list-item/product-list-item.component';
import { ProductListFilterComponent } from './components/products/products-list/product-list-filter/product-list-filter.component';
import { ProductListItemDetailComponent } from './components/products/products-list/product-list-item/product-list-item-detail/product-list-item-detail.component';
import { NgImageSliderModule } from 'ng-image-slider';
import { CartComponent } from './components/products/cart/cart.component';
import { CartItemComponent } from './components/products/cart/cart-item/cart-item.component';


registerLocaleData(localePt, 'pt');

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}


@NgModule({
  declarations: [
    AppComponent,
    UserInfoComponent,
    UserPreferencesListComponent,
    UserPreferencesSubListComponent,
    ProductsListComponent,
    ProductListItemComponent,
    ProductListFilterComponent,
    ProductListItemDetailComponent,
    CartComponent,
    CartItemComponent
  ],
  entryComponents: [
    UserPreferencesSubListComponent,
    ProductListFilterComponent,
    ProductListItemDetailComponent
  ],
  imports: [
    NgImageSliderModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    KeycloakAngularModule, 
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })],
  providers: [
    StatusBar,
    SplashScreen,
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService]
    },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
