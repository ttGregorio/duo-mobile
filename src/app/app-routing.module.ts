import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { UserInfoComponent } from './components/user/user-info/user-info.component';
import { UserPreferencesListComponent } from './components/user/user-preferences/user-preferences-list/user-preferences-list.component';
import { ProductsListComponent } from './components/products/products-list/products-list.component';
import { CartComponent } from './components/products/cart/cart.component';

const routes: Routes = [
  {path: '', component: ProductsListComponent,  canActivate: [AuthGuard]},
  {path: 'user-info', component: UserInfoComponent,  canActivate: [AuthGuard]},
  {path: 'user-preferences', component: UserPreferencesListComponent,  canActivate: [AuthGuard]},
  {path: 'cart', component: CartComponent,  canActivate: [AuthGuard]}

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
