import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product.model';
import { KeycloakService } from 'keycloak-angular';
import { ResponseApi } from 'src/app/model/response-api.model';
import { ProductType } from 'src/app/model/product-type.model';
import { ProductTypeService } from 'src/app/services/product-type.service';
import { ProductListFilterComponent } from './product-list-filter/product-list-filter.component';
import { ModalController } from '@ionic/angular';
import { ProductSearchDTO } from 'src/app/model/product-search-dto.model';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss'],
})
export class ProductsListComponent implements OnInit {

  productsList:Product[] = [];

  productTypesList:ProductType[] = [];

  username:string;

  productSearchDTO:ProductSearchDTO = new ProductSearchDTO('','','');

  slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  constructor(
    private modalController:ModalController,
    private keycloakService:KeycloakService,
    private productTypeService:ProductTypeService
  ) { }

  async ngOnInit() {
    this.username = await this.keycloakService.getUsername();

    this.findProductTypes();
  }

  findProductTypes(){
    this.productTypeService.findByUsername(this.username).subscribe((responseApi: ResponseApi) => {
      this.productTypesList = responseApi.data;
    }, err =>{
     /* this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });*/
    });
  }

  async openFilter(){
    const modal = await this.modalController.create({
      component: ProductListFilterComponent,
      componentProps: {
        'username': this.username
      }
    });
        
    modal.present();

    const { data } = await modal.onWillDismiss();

    if(data != null){
      this.productSearchDTO = data;
    }

    this.findProductTypes();
  }
}
