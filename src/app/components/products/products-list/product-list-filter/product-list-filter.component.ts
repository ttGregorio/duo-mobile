import { Component, OnInit, Input } from '@angular/core';
import { ProductSubTypeService } from 'src/app/services/product-sub-type.service';
import { ProductTypeService } from 'src/app/services/product-type.service';
import { ModalController } from '@ionic/angular';
import { Product } from 'src/app/model/product.model';
import { Partner } from 'src/app/model/partner.model';
import { ProductSearchDTO } from 'src/app/model/product-search-dto.model';

@Component({
  selector: 'app-product-list-filter',
  templateUrl: './product-list-filter.component.html',
  styleUrls: ['./product-list-filter.component.scss'],
})
export class ProductListFilterComponent implements OnInit {

  @Input() username: string;

  product:ProductSearchDTO = new ProductSearchDTO('','','');

  constructor(
    private modalController:ModalController
  ) { }

  ngOnInit() {}

  find() {
    this.modalController.dismiss(this.product);
  }

  dismiss() {
    this.modalController.dismiss();
  }
}
