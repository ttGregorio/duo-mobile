import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/model/product.model';
import { ModalController } from '@ionic/angular';
import { ProductImageService } from 'src/app/services/product-image.service';
import { ProductImage } from 'src/app/model/product-images.model';
import { ResponseApi } from 'src/app/model/response-api.model';
import { Order } from 'src/app/model/order.model';
import { Partner } from 'src/app/model/partner.model';
import { ProductType } from 'src/app/model/product-type.model';
import { ProductSubType } from 'src/app/model/product-sub-type.model';
import { OrderService } from 'src/app/services/order.service';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-product-list-item-detail',
  templateUrl: './product-list-item-detail.component.html',
  styleUrls: ['./product-list-item-detail.component.scss'],
})
export class ProductListItemDetailComponent implements OnInit {

  @Input() product:Product;

  order:Order = new Order(null,null,'',null,new Partner('','','','','',true),
  new ProductType('','',true, ''),new ProductSubType('','',null,true,''),
  new Product('','','',0,null,null,null,[], true),'',1,0,0,'OPEN','');
  
  imageObject: Array<object> = [];

  listImages:ProductImage[] = [];

  username:string;

  constructor(
    private keycloakService:KeycloakService,
    private modalController:ModalController,
    private productImageService:ProductImageService,
    private orderService:OrderService
  ) { }

async  ngOnInit() {
    this.username = await this.keycloakService.getUsername();

    this.order.product = this.product;
    this.order.unityValue = this.product.value;
    this.order.totalValue = this.product.value;
    console.log(this.product);
    console.log(this.order);

    this.loadImages();
  }

  reloadTotalValue(){
    this.order.totalValue = this.order.quantity * this.order.product.value;

    console.log(this.order);
  }

  loadImages(){
    this.productImageService.findAllList(this.product.id).subscribe((responseApi: ResponseApi) => {
      this.listImages = responseApi.data;

      for(var i:number = 0; i < this.listImages.length; i++){
        this.productImageService.findByName(this.product.id, this.listImages[i].name).subscribe((resq: ResponseApi ) =>{
          this.imageObject.push({
            image: resq.data ,
            thumbImage: resq.data,
            title: '',
            alt: ''
          });
        });
      }

      console.log(this.listImages);
  }, err => {
      
  });
  }

  dismiss() {
    this.modalController.dismiss();
  }

  buy(){
    this.order.partner.id = this.product.partner.id;
    this.order.username = this.username;
    this.order.productType = this.product.productType;
    this.order.productSubType = this.product.productSubType;

    this.orderService.createOrUpdate(this.order).subscribe((responseApi:ResponseApi)=> {
      this.modalController.dismiss(this.order);
    }, err =>{
      
    });
  }
}
