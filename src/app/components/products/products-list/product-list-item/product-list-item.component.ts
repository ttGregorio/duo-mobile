import { Component, OnInit, Input } from '@angular/core';
import { ProductType } from 'src/app/model/product-type.model';
import { Product } from 'src/app/model/product.model';
import { ProductService } from 'src/app/services/product.service';
import { KeycloakService } from 'keycloak-angular';
import { ResponseApi } from 'src/app/model/response-api.model';
import { ProductSearchDTO } from 'src/app/model/product-search-dto.model';
import { ModalController } from '@ionic/angular';
import { ProductListItemDetailComponent } from './product-list-item-detail/product-list-item-detail.component';
import { Order } from 'src/app/model/order.model';

@Component({
  selector: 'app-product-list-item',
  templateUrl: './product-list-item.component.html',
  styleUrls: ['./product-list-item.component.scss'],
})
export class ProductListItemComponent implements OnInit {

  @Input() productType:ProductType;

  @Input() productSearchDTO:ProductSearchDTO;

  @Input() order:Order;
  
  count:number = 2;

  page:number = 0;

  productsList:Product[] = [];

  username;

  slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  constructor(
    private modalController:ModalController,
    private keycloakService:KeycloakService,
    private productService:ProductService
  ) { }

  async ngOnInit() {
    this.username = await this.keycloakService.getUsername();

    this.loadProductList();
  }

  async openDetails(product:Product){
    const modal = await this.modalController.create({
      component: ProductListItemDetailComponent,
      componentProps: {
        'product': product
      }
    });
        
    modal.present();

    const { data } = await modal.onWillDismiss();

    if(data != null){
      this.order = data;
    }

    console.log(this.order);
  }

  loadProductList(){
    this.productService.findByUsername(this.page, this.count, this.productType.id, this.username, this.productSearchDTO).subscribe((responseApi: ResponseApi) => {
      for(var i = 0; i < responseApi.data.length; i++){
        this.productsList.push(responseApi.data[i]);
      }
    }, err =>{
     /* this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });*/
    });
  }

  moreData(){
    if(this.page != 0){
      console.log(this.page);
      this.productService.findByUsername(this.page, this.count, this.productType.id, this.username, this.productSearchDTO).subscribe((responseApi: ResponseApi) => {
        for(var i = 0; i < responseApi.data.length; i++){
          this.productsList.push(responseApi.data[i]);
        }
      }, err =>{
      /* this.showMessage({
          type:'error',
          text:err['error']['errors'][0]
        });*/
      });
    }
    this.page++;
  }
}
