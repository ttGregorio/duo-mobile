import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { OrderService } from 'src/app/services/order.service';
import { KeycloakService } from 'keycloak-angular';
import { ResponseApi } from 'src/app/model/response-api.model';
import { Order } from 'src/app/model/order.model';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  
  @ViewChild(IonInfiniteScroll, {static : true}) infiniteScroll: IonInfiniteScroll;

  teste:number = 0;

  items = [];

  orders:Order[] = [];

  username:string;

  count:number = 100;

  page:number = 0;

  constructor(
    private keycloakService:KeycloakService,
    private orderService:OrderService
  ) {}

  async ngOnInit(){
    this.username = await this.keycloakService.getUsername();

    for (var i = 0; i < 30; i++) {
      this.items.push( this.items.length );
    }

    this.loadOrdersByUsername();
  }

  loadData(event) {
    this.loadOrdersByUsername();
    this.page++;
    event.target.complete();
  }

  loadOrdersByUsername(){
    this.orderService.findByUsername(this.page, this.count, this.username).subscribe((responseApi: ResponseApi) => {
      for(var i = 0; i < responseApi.data.length; i++){
        this.orders.push(responseApi.data[i]);
      }
    }, err =>{
    });
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }
}
