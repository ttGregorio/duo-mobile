import { Component, OnInit, Input } from '@angular/core';
import { Order } from 'src/app/model/order.model';
import { ProductImageService } from 'src/app/services/product-image.service';
import { ResponseApi } from 'src/app/model/response-api.model';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss'],
})
export class CartItemComponent implements OnInit {

  @Input() order:Order;

  image:string;

  constructor(
    private productImageService:ProductImageService
  ) { }

  ngOnInit() {
    console.log(this.order);
    this.getImage();
  }

  
  getImage(){
    this.productImageService.findFirstImage(this.order.product.id).subscribe((responseApi: ResponseApi) => {
    this.image = responseApi.data;
    }, err =>{
    });
  }

}
