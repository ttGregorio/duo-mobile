import { Component, OnInit } from '@angular/core';
import { ProductTypeService } from 'src/app/services/product-type.service';
import { ResponseApi } from 'src/app/model/response-api.model';
import { ProductType } from 'src/app/model/product-type.model';
import { KeycloakService } from 'keycloak-angular';
import { UserPreferenceProductType } from 'src/app/model/user-preference-product-type.model';
import { UserPreferenceProductTypeService } from 'src/app/services/user-preference-product-type.service';
import { ModalController } from '@ionic/angular';
import { UserPreferencesSubListComponent } from '../user-preferences-sub-list/user-preferences-sub-list.component';

@Component({
  selector: 'app-user-preferences-list',
  templateUrl: './user-preferences-list.component.html',
  styleUrls: ['./user-preferences-list.component.scss'],
})
export class UserPreferencesListComponent implements OnInit {

  productTypesList:ProductType[] = [];

  username:string

  constructor(
    private modalController:ModalController,
    private keycloakService:KeycloakService,
    private productTypeService:ProductTypeService,
    private userPreferenceProductTypeService:UserPreferenceProductTypeService
  ) { }

  ngOnInit() {
    this.loadProductTypes();
  }

  async loadProductTypes(){
    this.username = await this.keycloakService.getUsername();

    this.productTypeService.findByUsername(this.username).subscribe((responseApi: ResponseApi) => {
      this.productTypesList = responseApi.data;
    }, err =>{
     /* this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });*/
    });
  }

  enableAll(productTypeId:string){
    this.userPreferenceProductTypeService.delete(productTypeId, this.username).subscribe((responseApi: ResponseApi) => {
      this.loadProductTypes();
    }, err =>{
     /* this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });*/
    });
  }

  disableAll(productTypeId:string){
    var userPreferenceProductType:UserPreferenceProductType = new UserPreferenceProductType(this.username, new ProductType(productTypeId,'',true,''));

    this.userPreferenceProductTypeService.createOrUpdate(userPreferenceProductType).subscribe((responseApi: ResponseApi) => {
      this.loadProductTypes();
    }, err =>{
     /* this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });*/
    });
  }

  openPreference(id:string){
    for(var i:number = 0; i < this.productTypesList.length; i++){
      if(this.productTypesList[i].id == id && this.productTypesList[i].color == "secondary"){
        this.presentModal(id);
      }
    }
  }

  async presentModal(id:string) {
    const modal = await this.modalController.create({
      component: UserPreferencesSubListComponent,
      componentProps: {
        'productTypeId': id
      }
    });
    return await modal.present();
  }
}
