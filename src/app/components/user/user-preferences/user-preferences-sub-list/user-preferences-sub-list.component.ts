import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ProductSubTypeService } from 'src/app/services/product-sub-type.service';
import { ProductType } from 'src/app/model/product-type.model';
import { ProductTypeService } from 'src/app/services/product-type.service';
import { ResponseApi } from 'src/app/model/response-api.model';
import { ProductSubType } from 'src/app/model/product-sub-type.model';
import { KeycloakService } from 'keycloak-angular';
import { UserPreferenceProductSubType } from 'src/app/model/user-preference-product-sub-type.model';
import { UserPreferenceProductSubTypeService } from 'src/app/services/user-preference-product-sub-type.service';

@Component({
  selector: 'app-user-preferences-sub-list',
  templateUrl: './user-preferences-sub-list.component.html',
  styleUrls: ['./user-preferences-sub-list.component.scss'],
})
export class UserPreferencesSubListComponent implements OnInit {

  @Input() productTypeId: string;
  
  productType:ProductType=new ProductType('','',true,'');

  productSubTypesList:ProductSubType[]=[];

  username:string;

  constructor(
    private keycloakService:KeycloakService,
    private modalController:ModalController,
    private productTypeService:ProductTypeService,
    private productSubTypeService:ProductSubTypeService,
    private userPreferenceProductSubTypeService:UserPreferenceProductSubTypeService
  ) { }

  ngOnInit() {
    this.findById();
    this.findProductSubTypes();
  }

  async findProductSubTypes(){
    this.username = await this.keycloakService.getUsername();

    this.productSubTypeService.findByUsername(this.username, this.productTypeId).subscribe((responseApi: ResponseApi) => {
      this.productSubTypesList = responseApi.data;
    }, err =>{
     /* this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });*/
    });
  }

  findById(){
    this.productTypeService.findById(this.productTypeId).subscribe((responseApi: ResponseApi) => {
      this.productType = responseApi.data;
    }, err =>{
     /* this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });*/
    });
  }

  desactive(idProductSubType:string){
     var userPreferenceProductType:UserPreferenceProductSubType = new UserPreferenceProductSubType(this.username, new ProductSubType(idProductSubType,'',new ProductType(this.productTypeId,'',true,''),true,''));

    this.userPreferenceProductSubTypeService.createOrUpdate(userPreferenceProductType).subscribe((responseApi: ResponseApi) => {
      this.findProductSubTypes();
    }, err =>{
     /* this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });*/
    });
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
