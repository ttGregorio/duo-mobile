import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user.model';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/services/user.service';
import { KeycloakService } from 'keycloak-angular';
import { ResponseApi } from 'src/app/model/response-api.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss'],
})
export class UserInfoComponent implements OnInit {

  user:User = new User('','','','','','','','');

  confirmPassword:string;
  
  constructor(
    private keycloakService:KeycloakService,
    private translate: TranslateService, 
    private userService:UserService, 

  ) { }

  ngOnInit() {
    this.findUserInfo();
  }

  async findUserInfo(){
    var username:string = await this.keycloakService.getUsername();

    this.userService.findByUsername(username).subscribe((responseApi: ResponseApi) => {
      this.user = responseApi.data;
    }, err =>{
     /* this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });*/
    });
  }

  send(){
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
  })
  //  this.user.profile = 'ROLE_COMPANY';
    this.userService.createOrUpdate(this.user).subscribe((responseApi:ResponseApi)=> {
      this.user  = new User('','','','','','', '', '');
      let userRet:User = responseApi.data;
      this.translate.get('userCreated').subscribe((res2: string) => {
        Toast.fire({
          icon: 'success',
          title: res2
        })
      });

 //     this.form.resetForm();
    }, err =>{/*
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });   */
    });
  }
}
