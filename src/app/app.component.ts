import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { UserService } from './services/user.service';
import { ResponseApi } from './model/response-api.model';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private keycloakService:KeycloakService, 
    private userService:UserService, 
    private translate: TranslateService,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    translate.addLangs(['en', 'es', 'pt']);
    translate.setDefaultLang('en');      
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  async ngOnInit() {
    var username:string = await this.keycloakService.getUsername();

    this.userService.findByUsername(username).subscribe((responseApi: ResponseApi) => {
      this.translate.use(responseApi.data.language);
    }, err =>{
    });
  }

  changeLanguage(language:string){
    this.translate.use(language);
  }
}
